﻿
namespace exa2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Select = new System.Windows.Forms.ComboBox();
            this.Cargar = new System.Windows.Forms.Button();
            this.Lim = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Unispace", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(425, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Resultado Del txt";
            // 
            // Select
            // 
            this.Select.FormattingEnabled = true;
            this.Select.Location = new System.Drawing.Point(321, 236);
            this.Select.Name = "Select";
            this.Select.Size = new System.Drawing.Size(537, 23);
            this.Select.TabIndex = 1;
            // 
            // Cargar
            // 
            this.Cargar.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Cargar.Location = new System.Drawing.Point(321, 421);
            this.Cargar.Name = "Cargar";
            this.Cargar.Size = new System.Drawing.Size(143, 47);
            this.Cargar.TabIndex = 3;
            this.Cargar.Text = "Cargar";
            this.Cargar.UseVisualStyleBackColor = true;
            this.Cargar.Click += new System.EventHandler(this.Cargar_Click);
            // 
            // Lim
            // 
            this.Lim.Font = new System.Drawing.Font("Unispace", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.Lim.Location = new System.Drawing.Point(711, 423);
            this.Lim.Name = "Lim";
            this.Lim.Size = new System.Drawing.Size(147, 45);
            this.Lim.TabIndex = 4;
            this.Lim.Text = "Limpiar";
            this.Lim.UseVisualStyleBackColor = true;
            this.Lim.Click += new System.EventHandler(this.Lim_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.Lim);
            this.Controls.Add(this.Cargar);
            this.Controls.Add(this.Select);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Select;
        private System.Windows.Forms.Button Cargar;
        private System.Windows.Forms.Button Lim;
    }
}

