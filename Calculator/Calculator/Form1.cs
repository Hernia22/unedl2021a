﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void botonsuma_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(textnum1.Text);
            int num2 = int.Parse(textnum2.Text);
            int resulta = num1 + num2;
            result.Text = String.Concat(resulta);
        }

        private void botonresta_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(textnum1.Text);
            int num2 = int.Parse(textnum2.Text);
            int resulta = num1 - num2;
            result.Text = String.Concat(resulta);
        }

        private void botonmulti_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(textnum1.Text);
            int num2 = int.Parse(textnum2.Text);
            int resulta = num1 * num2;
            result.Text = String.Concat(resulta);
        }

        private void botondivi_Click(object sender, EventArgs e)
        {
            if ((int.Parse(textnum2.Text) == 0) || (int.Parse(textnum1.Text) == 0))
            {
                MessageBox.Show("No se puede dividir entre 0");
                textnum1.Clear();
                textnum2.Clear();
            }
            else
            {
                int num1 = int.Parse(textnum1.Text);
                int num2 = int.Parse(textnum2.Text);
                int resulta = num1 / num2;
                result.Text = String.Concat(resulta);
            }
        }
    }
}






string nom1 = String.Format(txtBuscaNombre.Text);
            if (nom1 == personas[,]){
                result.Text = String.Concat(nom1);
            }
            else
            {
                MessageBox.Show("No se encuentra el nombre");
            }
