﻿
namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonsuma = new System.Windows.Forms.Button();
            this.botonresta = new System.Windows.Forms.Button();
            this.botonmulti = new System.Windows.Forms.Button();
            this.botondivi = new System.Windows.Forms.Button();
            this.botonc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textnum2 = new System.Windows.Forms.TextBox();
            this.textnum1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // botonsuma
            // 
            this.botonsuma.Location = new System.Drawing.Point(121, 197);
            this.botonsuma.Name = "botonsuma";
            this.botonsuma.Size = new System.Drawing.Size(75, 23);
            this.botonsuma.TabIndex = 0;
            this.botonsuma.Text = "Suma";
            this.botonsuma.UseVisualStyleBackColor = true;
            this.botonsuma.Click += new System.EventHandler(this.botonsuma_Click);
            // 
            // botonresta
            // 
            this.botonresta.Location = new System.Drawing.Point(234, 196);
            this.botonresta.Name = "botonresta";
            this.botonresta.Size = new System.Drawing.Size(75, 23);
            this.botonresta.TabIndex = 1;
            this.botonresta.Text = "Resta";
            this.botonresta.UseVisualStyleBackColor = true;
            this.botonresta.Click += new System.EventHandler(this.botonresta_Click);
            // 
            // botonmulti
            // 
            this.botonmulti.Location = new System.Drawing.Point(343, 196);
            this.botonmulti.Name = "botonmulti";
            this.botonmulti.Size = new System.Drawing.Size(75, 23);
            this.botonmulti.TabIndex = 2;
            this.botonmulti.Text = "Multiplicacion";
            this.botonmulti.UseVisualStyleBackColor = true;
            this.botonmulti.Click += new System.EventHandler(this.botonmulti_Click);
            // 
            // botondivi
            // 
            this.botondivi.Location = new System.Drawing.Point(446, 197);
            this.botondivi.Name = "botondivi";
            this.botondivi.Size = new System.Drawing.Size(75, 23);
            this.botondivi.TabIndex = 3;
            this.botondivi.Text = "Division";
            this.botondivi.UseVisualStyleBackColor = true;
            this.botondivi.Click += new System.EventHandler(this.botondivi_Click);
            // 
            // botonc
            // 
            this.botonc.Location = new System.Drawing.Point(314, 267);
            this.botonc.Name = "botonc";
            this.botonc.Size = new System.Drawing.Size(75, 23);
            this.botonc.TabIndex = 4;
            this.botonc.Text = "C";
            this.botonc.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Stencil", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(121, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ingrese el primer valor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Stencil", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(446, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "Ingrese el segundo valor";
            // 
            // textnum2
            // 
            this.textnum2.Location = new System.Drawing.Point(446, 98);
            this.textnum2.Name = "textnum2";
            this.textnum2.Size = new System.Drawing.Size(100, 23);
            this.textnum2.TabIndex = 7;
            // 
            // textnum1
            // 
            this.textnum1.Location = new System.Drawing.Point(121, 98);
            this.textnum1.Name = "textnum1";
            this.textnum1.Size = new System.Drawing.Size(100, 23);
            this.textnum1.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 351);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Resultado";
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Location = new System.Drawing.Point(213, 351);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(13, 15);
            this.result.TabIndex = 10;
            this.result.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.result);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textnum1);
            this.Controls.Add(this.textnum2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.botonc);
            this.Controls.Add(this.botondivi);
            this.Controls.Add(this.botonmulti);
            this.Controls.Add(this.botonresta);
            this.Controls.Add(this.botonsuma);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button botonsuma;
        private System.Windows.Forms.Button botonresta;
        private System.Windows.Forms.Button botonmulti;
        private System.Windows.Forms.Button botondivi;
        private System.Windows.Forms.Button botonc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textnum2;
        private System.Windows.Forms.TextBox textnum1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label result;
    }
}

